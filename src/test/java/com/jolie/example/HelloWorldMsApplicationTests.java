package com.jolie.example;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class HelloWorldMsApplicationTests {

	@Test
	void contextLoads() {
	}
	@Autowired
	public HelloWorldMsController hwController;
	@Test
	public void testSendGreeting() {
		String greeting = hwController.sendGreeting();
		assertEquals(greeting.substring(0, 11), "Greetings #");
	}

}
