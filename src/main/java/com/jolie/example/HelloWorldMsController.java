package com.jolie.example;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.Random;

@RestController
public class HelloWorldMsController {
	@RequestMapping("/greeting")
	public String sendGreeting(){
		Random rand = new Random();
    	int randNum = rand.nextInt(250);
		return "Greetings #"+randNum;
	}

}