FROM maven:3.6.3-jdk-11
VOLUME /tmp
ADD target/HelloWorldMS-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app.jar"]
